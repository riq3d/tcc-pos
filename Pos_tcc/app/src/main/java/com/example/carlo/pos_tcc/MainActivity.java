package com.example.carlo.pos_tcc;

//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.carlo.pos_tcc.bean.Usuario;
import com.example.carlo.pos_tcc.facade.UsuarioFacade;
import com.example.carlo.pos_tcc.service.UsuarioCallback;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Entrar(View view){
        EditText loginEdtEmail = findViewById(R.id.LoginEdtEmail);
        EditText loginEdtSenha = findViewById(R.id.LoginEdtSenha);
        if(loginEdtEmail.getText().toString().trim() == null ||
                loginEdtSenha.getText().toString().trim() == null ){
            Toast.makeText(MainActivity.this, "Preencha os campos.", Toast.LENGTH_LONG).show();
            return;
        }
        Usuario usuario = new Usuario();
        usuario.setEmail(loginEdtEmail.getText().toString().trim());
        usuario.setSenha(loginEdtSenha.getText().toString().trim());

        UsuarioFacade.verificarUsuario(usuario.getEmail(),usuario.getSenha(), new UsuarioCallback() {
            @Override
            public void onSuccess(Object obj) {
                Toast.makeText(MainActivity.this, "usuario cadastrado.", Toast.LENGTH_LONG).show();
                //System.out.println("--"+obj.toString());
                Usuario u = (Usuario)obj;
                System.out.println("obj ret: "+u.getEmail());
            }

            @Override
            public void onFailure(Throwable t) {

                System.out.println("deu erro: "+t.getMessage());
                Toast.makeText(MainActivity.this, "Nao cadastrado.", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void Cadastrar(View view){
        Usuario usuario = new Usuario();
        usuario.setEmail("email@email.com");
        usuario.setSenha("senhaaa");

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sdfDate.parse("1994-09-06");
         //   usuario.setDatanascimento(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        UsuarioFacade.cadastrarUsuario(usuario, new UsuarioCallback() {
            @Override
            public void onSuccess(Object obj) {
                Toast.makeText(MainActivity.this, "chegou no web service para cadastrar.", Toast.LENGTH_LONG).show();
                //System.out.println("--"+obj.toString());
                //Usuario usuario = (Usuario)obj;
                System.out.println("EMAIL RETORNADO: ");
                //System.out.println("DATA RETORNADO: "+usuario.getDatanascimento().toString());
            }

            @Override
            public void onFailure(Throwable t) {
                System.out.println("erro: "+t.getCause());
                System.out.println("erro: "+t.getLocalizedMessage());
                System.out.println("erro: "+t.getSuppressed());
                System.out.println("deu erro: "+t.getMessage());
                Toast.makeText(MainActivity.this, "Nao foi possível cadastrar.", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void Alterar(View view){
        Usuario a = new Usuario();
        a.setEmail("asda@asada");
        a.setSenha("as");
        UsuarioFacade.alterar(a, new UsuarioCallback() {
            @Override
            public void onSuccess(Object obj) {
                Toast.makeText(MainActivity.this, "chegou no web service para alterar.", Toast.LENGTH_LONG).show();
                //System.out.println("--"+obj.toString());
                //Usuario usuario = (Usuario)obj;
                System.out.println("funcao altera OK");
            }

            @Override
            public void onFailure(Throwable t) {
                System.out.println("deu erro: "+t.getMessage());
                Toast.makeText(MainActivity.this, "Nao foi possível cadastrar.", Toast.LENGTH_LONG).show();
            }
        });
    }
}
