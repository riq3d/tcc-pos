package com.example.carlo.pos_tcc.facade;

import com.example.carlo.pos_tcc.bean.Usuario;
import com.example.carlo.pos_tcc.service.UsuarioCallback;
import com.example.carlo.pos_tcc.service.UsuarioService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UsuarioFacade {

    public static void cadastrarUsuario(Usuario usuario, final UsuarioCallback callback){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(
                "http://192.168.43.176:18515/Tcc-pos/webresources/"
        ).addConverterFactory(GsonConverterFactory.create()).build();

        UsuarioService service = retrofit.create(UsuarioService.class);
        Call<Usuario> call = service.cadastrarUsuario(usuario);
        call.enqueue(new Callback<Usuario>() {
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                    System.out.println("retornou ok");
                } else {
                    System.out.println("STATUS CODE = " + response.code());
                    callback.onFailure(new Exception(response.errorBody().toString()));
                }
            }

            public void onFailure(Call<Usuario> call, Throwable t) {
                System.out.println("falhou");
                callback.onFailure(t);
            }
        });
    }

    public static void verificarUsuario(String email, String senha, final UsuarioCallback callback){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(
                "http://192.168.43.176:18515/Tcc-pos/webresources/"
        ).addConverterFactory(GsonConverterFactory.create()).build();

        UsuarioService service = retrofit.create(UsuarioService.class);
        Call<Usuario> call = service.verificarUsuario(email,senha);
        call.enqueue(new Callback<Usuario>() {
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    System.out.println("STATUS CODE = " + response.code());
                    callback.onFailure(new Exception(response.errorBody().toString()));
                }
            }

            public void onFailure(Call<Usuario> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    public static void alterar(Usuario usuario, final UsuarioCallback callback){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(
                "http://192.168.43.176:18515/Tcc-pos/webresources/"
        ).addConverterFactory(GsonConverterFactory.create()).build();

        UsuarioService service = retrofit.create(UsuarioService.class);
        Call<Usuario> call = service.alterar(usuario);
        call.enqueue(new Callback<Usuario>() {
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    System.out.println("STATUS CODE = " + response.code());
                    callback.onFailure(new Exception(response.errorBody().toString()));
                }
            }

            public void onFailure(Call<Usuario> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }
}
