package com.example.carlo.pos_tcc.service;

public interface UsuarioCallback<T> {
    public void onSuccess(T obj);
    public void onFailure(Throwable t);
}
