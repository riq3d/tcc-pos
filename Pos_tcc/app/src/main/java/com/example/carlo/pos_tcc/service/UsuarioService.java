package com.example.carlo.pos_tcc.service;

import com.example.carlo.pos_tcc.bean.Usuario;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UsuarioService {

    @POST("usuario/")
    Call<Usuario> cadastrarUsuario(@Body Usuario usuario);

    @GET("usuario/{email}/{senha}")
    Call<Usuario> verificarUsuario(@Path("email") String email,@Path("senha") String senha);

    //testando
    @PUT("usuario")
    Call<Usuario> alterar(@Body Usuario usuario);
}
