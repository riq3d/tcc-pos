package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_redirect_url_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_choose;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_otherwise;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_when_test;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_redirect_url_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_choose = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_otherwise = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_when_test = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_redirect_url_nobody.release();
    _jspx_tagPool_c_choose.release();
    _jspx_tagPool_c_otherwise.release();
    _jspx_tagPool_c_when_test.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"erro.jsp", true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!doctype html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("  <head>\n");
      out.write("    <title>S;esta</title>\n");
      out.write("    <meta charset=\"utf-8\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/custom-bs.css\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/jquery.fancybox.min.css\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/bootstrap-select.min.css\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"fonts/icomoon/style.css\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"fonts/line-icons/style.css\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/owl.carousel.min.css\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/animate.min.css\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/style.css\">    \n");
      out.write("        \n");
      out.write("   \n");
      out.write("    <!-- LIB FIREBASE -->\n");
      out.write("    <script src=\"https://www.gstatic.com/firebasejs/6.2.4/firebase-app.js\"></script>\n");
      out.write("    <!-- Add Firebase products that you want to use -->\n");
      out.write("    <script src=\"https://www.gstatic.com/firebasejs/6.2.4/firebase-auth.js\"></script>\n");
      out.write("    <script src=\"https://www.gstatic.com/firebasejs/6.2.4/firebase-database.js\"></script>\n");
      out.write("  </body>\n");
      out.write("  \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("  </head>\n");
      out.write("  <body id=\"top\">\n");
      out.write("\n");
      out.write("<div class=\"site-wrap\">\n");
      if (_jspx_meth_c_choose_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("    <!-- SCRIPTS -->\n");
      out.write("    <script src=\"js/jquery.min.js\"></script>\n");
      out.write("    <script src=\"js/bootstrap.bundle.min.js\"></script>\n");
      out.write("    <script src=\"js/isotope.pkgd.min.js\"></script>\n");
      out.write("    <script src=\"js/stickyfill.min.js\"></script>\n");
      out.write("    <script src=\"js/jquery.fancybox.min.js\"></script>\n");
      out.write("    <script src=\"js/jquery.easing.1.3.js\"></script>\n");
      out.write("    \n");
      out.write("    <script src=\"js/jquery.waypoints.min.js\"></script>\n");
      out.write("    <script src=\"js/jquery.animateNumber.min.js\"></script>\n");
      out.write("    <script src=\"js/owl.carousel.min.js\"></script>\n");
      out.write("    \n");
      out.write("    <script src=\"js/custom.js\"></script>  \n");
      out.write("    \n");
      out.write("    <script>\n");
      out.write("    var inputEmail = document.getElementById('email2');\n");
      out.write("    var inputPassword = document.getElementById('senha2');\n");
      out.write("    var btnLogin = document.getElementById('login');\n");
      out.write("\n");
      out.write("    var firebaseConfig = {\n");
      out.write("        apiKey: \"AIzaSyDeKP8dYDhhDElP6kxI6xqOTXMdZaM5NVg\",\n");
      out.write("        authDomain: \"tcc-pos-87a64.firebaseapp.com\",\n");
      out.write("        databaseURL: \"https://tcc-pos-87a64.firebaseio.com\",\n");
      out.write("        projectId: \"tcc-pos-87a64\",\n");
      out.write("        storageBucket: \"tcc-pos-87a64.appspot.com\",\n");
      out.write("        messagingSenderId: \"433779576620\",\n");
      out.write("        appId: \"1:433779576620:web:34bb258c7b959294\"\n");
      out.write("      };\n");
      out.write("    // Initialize Firebase\n");
      out.write("    firebase.initializeApp(firebaseConfig);\n");
      out.write("        \n");
      out.write("    btnLogin.addEventListener('click', function (){\n");
      out.write("            \n");
      out.write("        firebase.auth().signInWithEmailAndPassword(inputEmail.value, inputPassword.value).then(function (result){\n");
      out.write("            //alert('conectado no firebase');\n");
      out.write("            alert('conectado no firebase. email: '+inputEmail.value);\n");
      out.write("            document.getElementById(\"formLogin\").action=\"./LoginServlet?action=login\";\n");
      out.write("            document.getElementById(\"formLogin\").method = \"POST\";\n");
      out.write("            document.getElementById(\"formLogin\").email.value = inputEmail.value;\n");
      out.write("            document.getElementById(\"formLogin\").senha.value = inputPassword.value;\n");
      out.write("            document.getElementById(\"formLogin\").submit();  \n");
      out.write("            \n");
      out.write("        }).catch(function(error) {        \n");
      out.write("            alert('nao conectado');\n");
      out.write("            // Handle Errors here.\n");
      out.write("            var errorCode = error.code;\n");
      out.write("            var errorMessage = error.message;\n");
      out.write("            // ...\n");
      out.write("            console.log(errorCode);\n");
      out.write("            console.log(errorMessage);\n");
      out.write("        });\n");
      out.write("\n");
      out.write("      /*  firebase.auth().onAuthStateChanged(function(user) {\n");
      out.write("            if (user) {\n");
      out.write("              // User is signed in.\n");
      out.write("              alert('ja esta logado depois de chamar a funcao signInWithEmailAndPassword');\n");
      out.write("            } else {\n");
      out.write("              // No user is signed in.\n");
      out.write("              alert('n esta logado depois de chamar a funcao signInWithEmailAndPassword');\n");
      out.write("            }\n");
      out.write("        });  */    \n");
      out.write("    });\n");
      out.write("\n");
      out.write("    </script> \n");
      out.write("    \n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_choose_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:choose
    org.apache.taglibs.standard.tag.common.core.ChooseTag _jspx_th_c_choose_0 = (org.apache.taglibs.standard.tag.common.core.ChooseTag) _jspx_tagPool_c_choose.get(org.apache.taglibs.standard.tag.common.core.ChooseTag.class);
    _jspx_th_c_choose_0.setPageContext(_jspx_page_context);
    _jspx_th_c_choose_0.setParent(null);
    int _jspx_eval_c_choose_0 = _jspx_th_c_choose_0.doStartTag();
    if (_jspx_eval_c_choose_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("  ");
        if (_jspx_meth_c_when_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_choose_0, _jspx_page_context))
          return true;
        out.write('\n');
        if (_jspx_meth_c_otherwise_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_choose_0, _jspx_page_context))
          return true;
        out.write(' ');
        out.write('\n');
        int evalDoAfterBody = _jspx_th_c_choose_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_choose_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_choose.reuse(_jspx_th_c_choose_0);
      return true;
    }
    _jspx_tagPool_c_choose.reuse(_jspx_th_c_choose_0);
    return false;
  }

  private boolean _jspx_meth_c_when_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_choose_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:when
    org.apache.taglibs.standard.tag.rt.core.WhenTag _jspx_th_c_when_0 = (org.apache.taglibs.standard.tag.rt.core.WhenTag) _jspx_tagPool_c_when_test.get(org.apache.taglibs.standard.tag.rt.core.WhenTag.class);
    _jspx_th_c_when_0.setPageContext(_jspx_page_context);
    _jspx_th_c_when_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_choose_0);
    _jspx_th_c_when_0.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${!sessionScope.logado}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_when_0 = _jspx_th_c_when_0.doStartTag();
    if (_jspx_eval_c_when_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("    \n");
        out.write("    <!-- HOME -->\n");
        out.write("    <section class=\"home-section section-hero overlay bg-image\" style=\"background-image: url('images/hero_1.jpg');\" id=\"home-section\">\n");
        out.write("\n");
        out.write("      <div class=\"container\">\n");
        out.write("        <div class=\"row align-items-center justify-content-center\">\n");
        out.write("          <div class=\"col-md-12\">\n");
        out.write("            <div class=\"mb-5 text-center\">\n");
        out.write("              <h1 class=\"text-white font-weight-bold\">S;ESTA</h1>\n");
        out.write("            </div>             \n");
        out.write("              \n");
        out.write("            <!--<form id=\"formLogin\" method=\"post\" action=\"LoginServlet?action=login\" class=\"search-jobs-form\">        -->\n");
        out.write("            <form id=\"formLogin\" class=\"search-jobs-form\"> \n");
        out.write("                <input id=\"email\" name=\"email\" type=\"text\" style=\"visibility: hidden\">\n");
        out.write("                <input id=\"senha\" name=\"senha\" type=\"password\" style=\"visibility: hidden\">\n");
        out.write("            </form>              \n");
        out.write("              <div class=\"row mb-5\">\n");
        out.write("                <div class=\"col-12 col-sm-6 col-md-6 col-lg-6 mb-6 mb-lg-0\">\n");
        out.write("                  <p class=\"text-center\">Digite seu e-mail:</p>\n");
        out.write("                </div>\n");
        out.write("                <div class=\"col-12 col-sm-6 col-md-6 col-lg-6 mb-6 mb-lg-0\">\n");
        out.write("                  <input id=\"email2\" name=\"email\" type=\"text\" class=\"form-control form-control-lg\" placeholder=\"E-mail\">\n");
        out.write("                </div>\n");
        out.write("              </div>\n");
        out.write("              <div class=\"row mb-5\">\n");
        out.write("                <div class=\"col-12 col-sm-6 col-md-6 col-lg-6 mb-6 mb-lg-0\">\n");
        out.write("                  <p class=\"text-center\">Digite sua senha:</p>\n");
        out.write("                </div>\n");
        out.write("                <div class=\"col-12 col-sm-6 col-md-6 col-lg-6 mb-6 mb-lg-0\">\n");
        out.write("                  <input id=\"senha2\" name=\"senha\" type=\"password\" class=\"form-control form-control-lg\" placeholder=\"Senha\">\n");
        out.write("                </div>\n");
        out.write("              </div>\n");
        out.write("              <div class=\"d-flex justify-content-center\">\n");
        out.write("                  <button id=\"login\" type=\"submit\" class=\"btn btn-primary btn-lg text-white btn-search\">\n");
        out.write("                      <span class=\"icon-search icon mr-2\"></span>\n");
        out.write("                      Entrar\n");
        out.write("                  </button>\n");
        out.write("              </div>                \n");
        out.write("               \n");
        out.write("          </div>\n");
        out.write("        </div>\n");
        out.write("      </div> \n");
        out.write("    </section>\n");
        out.write("  </div>\n");
        int evalDoAfterBody = _jspx_th_c_when_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_when_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_0);
      return true;
    }
    _jspx_tagPool_c_when_test.reuse(_jspx_th_c_when_0);
    return false;
  }

  private boolean _jspx_meth_c_otherwise_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_choose_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:otherwise
    org.apache.taglibs.standard.tag.common.core.OtherwiseTag _jspx_th_c_otherwise_0 = (org.apache.taglibs.standard.tag.common.core.OtherwiseTag) _jspx_tagPool_c_otherwise.get(org.apache.taglibs.standard.tag.common.core.OtherwiseTag.class);
    _jspx_th_c_otherwise_0.setPageContext(_jspx_page_context);
    _jspx_th_c_otherwise_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_choose_0);
    int _jspx_eval_c_otherwise_0 = _jspx_th_c_otherwise_0.doStartTag();
    if (_jspx_eval_c_otherwise_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\n");
        out.write("  <!-- <p>You're not logged in!</p> -->\n");
        out.write("  ");
        if (_jspx_meth_c_redirect_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_otherwise_0, _jspx_page_context))
          return true;
        out.write('\n');
        int evalDoAfterBody = _jspx_th_c_otherwise_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_otherwise_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_otherwise.reuse(_jspx_th_c_otherwise_0);
      return true;
    }
    _jspx_tagPool_c_otherwise.reuse(_jspx_th_c_otherwise_0);
    return false;
  }

  private boolean _jspx_meth_c_redirect_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_otherwise_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:redirect
    org.apache.taglibs.standard.tag.rt.core.RedirectTag _jspx_th_c_redirect_0 = (org.apache.taglibs.standard.tag.rt.core.RedirectTag) _jspx_tagPool_c_redirect_url_nobody.get(org.apache.taglibs.standard.tag.rt.core.RedirectTag.class);
    _jspx_th_c_redirect_0.setPageContext(_jspx_page_context);
    _jspx_th_c_redirect_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_otherwise_0);
    _jspx_th_c_redirect_0.setUrl("/LoginServlet?action=list");
    int _jspx_eval_c_redirect_0 = _jspx_th_c_redirect_0.doStartTag();
    if (_jspx_th_c_redirect_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_redirect_url_nobody.reuse(_jspx_th_c_redirect_0);
      return true;
    }
    _jspx_tagPool_c_redirect_url_nobody.reuse(_jspx_th_c_redirect_0);
    return false;
  }
}
