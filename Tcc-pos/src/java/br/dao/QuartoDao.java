package br.dao;

import br.bean.Adicional;
import br.bean.Horario;
import br.bean.Quarto;
import br.connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.postgresql.util.PSQLException;
import org.postgresql.util.PSQLState;

public class QuartoDao {
    // a conexão com o banco de dados
    private Connection connection;
    private final String sqlSelectAllQuartosById = "select * from quarto t1 WHERE t1.id_hospedagem = ? " +
                    "LIMIT ? OFFSET ?";    
    private final String sqlCountAllQuartoPorHotel = "select count(*) as qtd from quarto WHERE id_hospedagem = ?";
    private final String sqlSelectQuartoById = "select t1.*,t2.*,t3.*,t4.f1,t4.f2,t4.f3 from quarto t1 " +
"LEFT join quarto_horas t2 " +
"on t1.id_quarto = t2.id_quarto " +
"LEFT join horas t3 " +
"on t2.id_horas = t3.id_horas " +
"LEFT join (" +
"	select id_quarto, " +
"  	max(case when seq='1' then id_firebase end) as f1, " +
"  	max(case when seq='2' then id_firebase end) as f2, " +
"  	max(case when seq='3' then id_firebase end) as f3 " +
"	FROM (" +
"		select tt.*, row_number() OVER (PARTITION by 0)::INTEGER as seq from imagem as tt " +
"		where id_quarto = ? " +
"	) as t " +
"	group by 1" +
")as t4 on t1.id_quarto = t4.id_quarto " +
"WHERE t1.id_quarto = ?";
    private final String sqlInsertQuarto = "insert into quarto (id_hotel,nome,classificacao,preco,dt_cadastro,dt_revisao)"
            + " values (?,?,?,?,current_date,current_date);";
    private final String sqlSelectQuartoByIdWithComp = "select " +
"  t1.id_quarto, t1.nome, t1.preco, t2.ad1, t2.ad2, t2.ad3, t3.f1, t3.f2, t3.f3 " +
"from quarto t1 " +
"LEFT join (" +
"	select id_quarto," +
"  	max(case when seq='1' then id_adicional end) as ad1," +
"  	max(case when seq='2' then id_adicional end) as ad2," +
"  	max(case when seq='3' then id_adicional end) as ad3" +
"	FROM (" +
"		select tt.*, row_number() OVER (PARTITION by 0)::INTEGER as seq from quarto_adicional as tt" +
"		where id_quarto = ?" +
"	) as t" +
"	group by 1" +
")as t2 on t1.id_quarto = t2.id_quarto " +
"LEFT join (" +
"	select id_quarto," +
"  	max(case when seq='1' then id_firebase end) as f1," +
"  	max(case when seq='2' then id_firebase end) as f2," +
"  	max(case when seq='3' then id_firebase end) as f3" +
"	FROM (" +
"		select tt.*, row_number() OVER (PARTITION by 0)::INTEGER as seq from imagem as tt" +
"		where id_quarto = ?" +
"	) as t " +
"	group by 1" +
")as t3 on t1.id_quarto = t3.id_quarto;";
    
    public QuartoDao() {
        this.connection = new ConnectionFactory().getConnection();
    }
    
    public List<Quarto> getQuartosByIdHotels(int id, int currentPage, int recordsPerPage) {
        try{            
            int start = currentPage * recordsPerPage - recordsPerPage;
            //System.out.println("start: "+start);
            PreparedStatement stmt = connection.prepareStatement(sqlSelectAllQuartosById);
            stmt.setInt(1, id);            
            stmt.setInt(2, recordsPerPage);
            stmt.setInt(3, start);
            
            System.out.println("SQL ANTES PAGINACAO:");
            System.out.println(stmt.toString()); //visualizar a query de consulta
            List<Quarto> listaQuartos = new ArrayList<>();
            
            // executa um select
            ResultSet rs = stmt.executeQuery();            
            while ( rs.next() ){
                Quarto quarto = new Quarto();
                quarto.setId(rs.getInt("id_quarto")); 
                quarto.setIdHospedagem(rs.getInt("id_hospedagem")); 
                quarto.setNome(rs.getString("nome"));   
                quarto.setPreco(rs.getDouble("preco")); 
              
                listaQuartos.add(quarto);
            }

            stmt.close();
            connection.close();
            return listaQuartos;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    public Quarto getQuartoById(int id) {
        try{            
            PreparedStatement stmt = connection.prepareStatement(sqlSelectQuartoById);
            stmt.setInt(1, id);        
            stmt.setInt(2, id);       
            
            System.out.println(stmt.toString()); //visualizar a query de consulta
            Quarto quarto = null;
            
            // executa um select
            ResultSet rs = stmt.executeQuery();            
            while ( rs.next() ){
                quarto = new Quarto();
                quarto.setId(rs.getInt("id_quarto")); 
                quarto.setIdHospedagem(rs.getInt("id_hospedagem")); 
                quarto.setNome(rs.getString("nome"));   
                quarto.setPreco(rs.getDouble("preco")); 
                List<String> listaFotos = new ArrayList<>();
                listaFotos.add(rs.getString("f1"));
                listaFotos.add(rs.getString("f2"));
                listaFotos.add(rs.getString("f3"));
                quarto.setListaImagemFirebase(listaFotos);   
                /*
                List<Horario> listaHorarios = new ArrayList<>();
                //System.out.println("hora q vem do bd: "+rs.getTime("horario"));             
                SimpleDateFormat ft = new SimpleDateFormat ("HH:mm:ss");
                //System.out.println(" Date formatada: " + ft.format(rs.getTime("horario")));              
                Date a = ft.parse(ft.format(rs.getTime("horario")));
                //System.out.println("hora formatada para date: "+a);
                Horario horario = new Horario();
                horario.setIdHorario(rs.getInt("id_horas"));
                horario.setDisponivel(rs.getBoolean("disponivel"));
                horario.setHorario(a);
                listaHorarios.add(horario);*/
            }
            stmt.close();
            connection.close();
            return quarto;
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("deu erro ao buscar quarto: "+e.toString());
        }
        return null;
    }
    
    public int getQuantidadeTotalHotel(int id) {
        try{            
            //System.out.println("dentro do dao: "+user.getLogin());
            PreparedStatement stmt = connection.prepareStatement(sqlCountAllQuartoPorHotel);
            stmt.setInt(1, id);       
             
            int quantidadeTotal = 0;
            // executa um select
            ResultSet rs = stmt.executeQuery();            
            while ( rs.next() ){
                quantidadeTotal = rs.getInt("qtd"); 
            }
            
            stmt.close();
            connection.close();
            return quantidadeTotal;
        }catch(Exception e){
            e.printStackTrace();
        }
        return 0;
    }
    
    public int adicionaQuarto(Quarto quarto) throws PSQLException, Exception{        
        try {
            // prepared statement para inserção
            PreparedStatement stmt = connection.prepareStatement(sqlInsertQuarto, Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, quarto.getIdHospedagem());
            stmt.setString(2, quarto.getNome());
            stmt.setDouble(3, quarto.getClassificacao());
            stmt.setDouble(4, quarto.getPreco());
            
            // executa
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Insert room failed, no rows affected.");
            }
            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    //System.out.println("ID do QUARTO inserido: "+generatedKeys.getLong(1));
                    Long id = generatedKeys.getLong(1);
                    stmt.close();
                    connection.close();
                    return id.intValue();                    
                }
                else {
                    throw new SQLException("Insert room failed, no ID obtained.");
                }
            }               
        } catch (PSQLException e) {
            System.out.println("erro dentro do dao! "+e);            
            e.getMessage();
            throw new PSQLException("Chave duplicada", PSQLState.NO_DATA);
        }catch(Exception e){
            e.getMessage();
            throw new RuntimeException(e);
        }
    }
    
    public Quarto getQuartoByIdWithComp(int id) {
        try{            
            PreparedStatement stmt = connection.prepareStatement(sqlSelectQuartoByIdWithComp);
            stmt.setInt(1, id);     
            stmt.setInt(2, id);     
            
            //System.out.println(stmt.toString()); //visualizar a query de consulta
            Quarto quarto = null;
            
            // executa um select
            ResultSet rs = stmt.executeQuery();            
            while ( rs.next() ){
                quarto = new Quarto();
                quarto.setId(rs.getInt("id_quarto")); 
                quarto.setNome(rs.getString("nome"));   
                quarto.setPreco(rs.getDouble("preco")); 
                
                List<Adicional> listaAdicionais = new ArrayList<>();
                listaAdicionais.add(new Adicional(rs.getInt("ad1")));
                listaAdicionais.add(new Adicional(rs.getInt("ad2")));
                listaAdicionais.add(new Adicional(rs.getInt("ad3")));
                quarto.setListaAdicionais(listaAdicionais);
                List<String> listaFotos = new ArrayList<>();
                listaFotos.add(rs.getString("f1"));
                listaFotos.add(rs.getString("f2"));
                listaFotos.add(rs.getString("f3"));
                quarto.setListaImagemFirebase(listaFotos);             
            }
            stmt.close();
            connection.close();
            return quarto;
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("deu erro ao buscar quarto: "+e.toString());
        }
        return null;
    }
}
