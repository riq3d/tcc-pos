package br.dao;

import br.bean.Reserva;
import br.connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.postgresql.util.PSQLException;
import org.postgresql.util.PSQLState;

public class ReservaDao {
    private Connection connection;
    private final String sqlReservaByIds = "select " +
                "t1.*, t2.nome AS nome_pessoa, t3.nome AS nome_quarto, t3.preco AS preco_quarto, t4.horario, t5.disponibilidade, t3.id_hotel " +
                "from reserva t1 inner join pessoa t2 " +
                "on t1.id_pessoa = t2.id_pessoa " +
                "inner join quarto t3 on t1.id_quarto = t3.id_quarto " +
                "inner join horas t4 on t1.id_hr_chegada = t4.id_horas " +
                "inner join quarto_horas t5 on t1.id_hr_chegada = t5.id_horas AND t1.id_quarto = t5.id_quarto " +
                "WHERE t1.dt_reserva = CURRENT_DATE AND t1.id_hr_chegada = ? AND t1.id_quarto = ? ";    
    private final String sqlCheckIn = "SELECT t1.*,t2.nome AS nome_pessoa, t3.*, t4.preco, t5.horario " +
                "FROM reserva t1 " +
                "INNER JOIN pessoa t2 ON t1.id_pessoa = t2.id_pessoa " +
                "INNER JOIN quarto_horas t3 ON t1.id_hr_chegada = t3.id_horas AND t1.id_quarto = t3.id_quarto " +
                "INNER JOIN quarto t4 ON t1.id_quarto = t4.id_quarto " +
                "INNER JOIN horas t5 ON t1.id_hr_chegada = t5.id_horas " + 
                "WHERE DT_RESERVADA = CURRENT_DATE " +
                "AND t1.id_hr_chegada = ? AND t1.id_quarto = ? AND t1.hr_saida is null AND pago = false";
    private final String sqlFecharPagamento = "insert into reserva_pagamento " +
                "(id_forma_pagamento, id_reserva, preco) values (?,?,?);";
    private final String sqlFecharReserva = "update reserva " +
                "set hr_saida = current_time, pago=true " +
                "WHERE id_reserva = ?";
    private final String sqlUpdateQuartoHoras = "update quarto_horas " +
                "set disponibilidade = ? " +
                "where id_quarto=? and id_horas = ? and id_hotel = ? AND dt_reservada = ?";
    
    public ReservaDao() {
        this.connection = new ConnectionFactory().getConnection();
    }
    
    public Reserva getReserva(int idQuarto, int idHorario) {
        try{            
            PreparedStatement stmt = connection.prepareStatement(sqlReservaByIds);
            stmt.setInt(1, idHorario);   
            stmt.setInt(2, idQuarto);   
            
            //System.out.println("sql da reserva: ");
            //System.out.println(stmt.toString()); //visualizar a query de consulta
            Reserva quarto = null;
            
            // executa um select
            ResultSet rs = stmt.executeQuery();            
            while ( rs.next() ){
                quarto = new Reserva();
                quarto.setIdReserva(rs.getInt("id_reserva")); 
                quarto.setIdHotel(rs.getInt("id_hotel")); 
                quarto.setIdQuarto(rs.getInt("id_quarto")); 
                quarto.setIdPessoa(rs.getInt("id_pessoa")); 
                quarto.setIdHrChegada(rs.getInt("id_hr_chegada")); 
                
                SimpleDateFormat ft = new SimpleDateFormat ("HH:mm:ss");
                try{                    
                    Date a = ft.parse(ft.format(rs.getTime("hr_saida")));
                    quarto.setHrSaida(a);
                }catch(Exception e){
                    System.out.println("erro ao formatar data HR_SAIDA");
                }
                       
                quarto.setPago(rs.getBoolean("pago"));   
                quarto.setPreco(rs.getDouble("preco_quarto")); 
                quarto.setNomePessoa(rs.getString("nome_pessoa")); 
                quarto.setNomeQuarto(rs.getString("nome_quarto"));
                quarto.setDisponibilidade(rs.getString("disponibilidade"));
                try{                    
                    Date a2 = ft.parse(ft.format(rs.getTime("horario")));
                    quarto.setHorarioEntrada(a2);
                }catch(Exception e){
                    System.out.println("erro ao formatar data HORARIO");
                }               
                
                quarto.setDtReserva(rs.getDate("dt_reserva"));
                
            }
            stmt.close();
            connection.close();
            return quarto;
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("deu erro ao buscar reserva: "+e.toString());
        }
        return null;
    }
    
    public Reserva realizarCheckIn(int idQuarto, int idHorario) {
        try{            
            PreparedStatement stmt = connection.prepareStatement(sqlCheckIn);
            stmt.setInt(1, idHorario);   
            stmt.setInt(2, idQuarto);   
            
            //System.out.println("sql do check-in: ");
            //System.out.println(stmt.toString()); //visualizar a query de consulta
            Reserva reserva = null;
            
            // executa um select
            ResultSet rs = stmt.executeQuery();            
            while ( rs.next() ){
                reserva = new Reserva();
                reserva.setIdReserva(rs.getInt("id_reserva")); 
                reserva.setIdQuarto(rs.getInt("id_quarto")); 
                reserva.setIdPessoa(rs.getInt("id_pessoa")); 
                reserva.setIdHrChegada(rs.getInt("id_hr_chegada")); 
                
                SimpleDateFormat ft = new SimpleDateFormat ("HH:mm:ss");
                try{                    
                    Date a = ft.parse(ft.format(rs.getTime("hr_saida")));
                    reserva.setHrSaida(a);
                }catch(Exception e){
                    System.out.println("erro ao formatar data HR_SAIDA");
                }
                       
                reserva.setPago(rs.getBoolean("pago"));                 
                reserva.setNomePessoa(rs.getString("nome_pessoa")); 
                reserva.setIdHotel(rs.getInt("id_hotel")); 
                
                try{                    
                    Date a2 = ft.parse(ft.format(rs.getTime("horario")));
                    reserva.setHorarioEntrada(a2);
                }catch(Exception e){
                    System.out.println("erro ao formatar data HORARIO");
                }               
                
                reserva.setDisponibilidade(rs.getString("disponibilidade"));
                reserva.setDtReserva(rs.getDate("dt_reservada"));
                reserva.setPreco(rs.getDouble("preco")); 
            }
            stmt.close();
            connection.close();
            return reserva;
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("deu erro ao buscar reserva: "+e.toString());
        }
        return null;
    }
    
    public void fecharPagamento(int idFormaPagamento, int idReserva, Double preco) throws PSQLException, Exception{        
        try {
            // prepared statement para inserção
            PreparedStatement stmt = connection.prepareStatement(sqlFecharPagamento);
            stmt.setInt(1, idFormaPagamento);
            stmt.setInt(2, idReserva);
            stmt.setDouble(3, preco);
            
            // executa
            stmt.executeUpdate();
            stmt.close();
            connection.close();
        } catch (PSQLException e) {
            System.out.println("erro dentro do dao! "+e);            
            e.getMessage();
            throw new PSQLException("Chave duplicada", PSQLState.NO_DATA);
        }catch(Exception e){
            e.getMessage();
            throw new RuntimeException(e);
        }
    }
    
    public void fecharReserva(int idReserva) throws PSQLException, Exception{        
        try {
            // prepared statement para inserção
            PreparedStatement stmt = connection.prepareStatement(sqlFecharReserva);
            stmt.setInt(1, idReserva);
         
            // executa
            stmt.executeUpdate();
            stmt.close();
            connection.close();
        } catch (PSQLException e) {
            System.out.println("erro dentro do dao! "+e);            
            e.getMessage();
            throw new PSQLException("Chave duplicada", PSQLState.NO_DATA);
        }catch(Exception e){
            e.getMessage();
            throw new RuntimeException(e);
        }
    }
    
    public void quartoOcupado(String disponibilidade, int idQuarto, int idHorario, int idHotel, Date dtReservada)
            throws PSQLException, Exception{        
        try {
            // prepared statement para inserção
            PreparedStatement stmt = connection.prepareStatement(sqlUpdateQuartoHoras);
            stmt.setString(1, disponibilidade);
            stmt.setInt(2, idQuarto);
            stmt.setInt(3, idHorario);
            stmt.setInt(4, idHotel);
            
            //converte de java.util.date para java.sql.date
            //java.sql.Date data = new java.sql.Date(dtReservada.getTime());
            stmt.setDate(5, new java.sql.Date(dtReservada.getTime()));            
            //stmt.setDate(5, dtReservada);
            System.out.println("dt transf: "+new java.sql.Date(dtReservada.getTime()));
            System.out.println("sql do check-in: ");
            System.out.println(stmt.toString()); //visualizar a query de consulta
            
            // executa
            stmt.executeUpdate();
            stmt.close();
            connection.close();
        } catch (PSQLException e) {
            System.out.println("erro dentro do dao! "+e);            
            e.getMessage();
            throw new PSQLException("Chave duplicada", PSQLState.NO_DATA);
        }catch(Exception e){
            e.getMessage();
            throw new RuntimeException(e);
        }
    }
}
