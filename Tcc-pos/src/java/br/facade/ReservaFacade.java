package br.facade;

import br.bean.Reserva;
import br.dao.ReservaDao;
import java.util.Date;
import org.postgresql.util.PSQLException;

public class ReservaFacade {
    public static Reserva getReserva(int idQuarto, int idHorario) throws PSQLException, Exception{
        try{
            ReservaDao dao = new ReservaDao();
            return dao.getReserva(idQuarto,idHorario);        
        }catch(RuntimeException e){
            System.out.println("erro facade runtime. "+e);
            throw new RuntimeException();
        }catch(Exception e){
            System.out.println("erro geral facade. "+e);
            throw new Exception();
        }
    }
    
    public static Reserva realizarCheckIn(int idQuarto, int idHorario) throws PSQLException, Exception{
        try{
            ReservaDao dao = new ReservaDao();
            return dao.realizarCheckIn(idQuarto,idHorario);        
        }catch(RuntimeException e){
            System.out.println("erro facade runtime. "+e);
            throw new RuntimeException();
        }catch(Exception e){
            System.out.println("erro geral facade. "+e);
            throw new Exception();
        }
    }
    
    public static void fecharPagamento(int idFormaPagamento, int idReserva, Double preco) throws PSQLException, Exception{
        try{
            ReservaDao dao = new ReservaDao();
            dao.fecharPagamento(idFormaPagamento, idReserva, preco);        
        }catch(RuntimeException e){
            System.out.println("erro facade runtime. "+e);
            throw new RuntimeException();
        }catch(Exception e){
            System.out.println("erro geral facade. "+e);
            throw new Exception();
        }
    }
    
    public static void fecharReserva(int idReserva) throws PSQLException, Exception{
        try{
            ReservaDao dao = new ReservaDao();
            dao.fecharReserva(idReserva);        
        }catch(RuntimeException e){
            System.out.println("erro facade runtime. "+e);
            throw new RuntimeException();
        }catch(Exception e){
            System.out.println("erro geral facade. "+e);
            throw new Exception();
        }
    }
    
    public static void quartoOcupado(String disponibilidade, int idQuarto, int idHorario, int idHotel, Date dtReservada) 
            throws PSQLException, Exception{
        try{
            ReservaDao dao = new ReservaDao();
            dao.quartoOcupado(disponibilidade,idQuarto, idHorario, idHotel, dtReservada);        
        }catch(RuntimeException e){
            System.out.println("erro facade runtime. "+e);
            throw new RuntimeException();
        }catch(Exception e){
            System.out.println("erro geral facade. "+e);
            throw new Exception();
        }
    }
}
