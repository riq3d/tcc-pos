package br.servlet;

import br.bean.Usuario;
import br.facade.UsuarioFacade;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name="UsuarioServlet", urlPatterns={"/UsuarioServlet"})
public class UsuarioServlet extends HttpServlet {
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        RequestDispatcher rd;
        try {
            HttpSession session = request.getSession();
            System.out.println("se quer chegou aqui??");
            String action = request.getParameter("action");
            if ( session.getAttribute("Usuario") == null){              
                //System.out.println("oq tem no action: "+action);
                if ( "login".equalsIgnoreCase(action) ){
                    String email = request.getParameter("email");
                    if(email.equals("")) return;
                    String senha = request.getParameter("senha");    
                    if(senha.equals("")) return;
                    //System.out.println("email digitado: "+email);
                    
                    MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
                    byte messageDigest[] = algorithm.digest(senha.getBytes("UTF-8"));

                    StringBuilder hexString = new StringBuilder();
                    for (byte b : messageDigest) {
                      hexString.append(String.format("%02X", 0xFF & b));
                    }
                    String senhaCriptografada = hexString.toString();
                    //System.out.println("senha crip: "+senhaCriptografada);
                    //puxar um dAO aki
                    Usuario usuario = new Usuario(email,senhaCriptografada);
                    Usuario u = UsuarioFacade.verificaUsuario(usuario);
                    if( u != null){
                        //System.out.println("encontrou!! Usuario logado!");
                        session.setAttribute("Usuario", u);        
                        session.setAttribute("logado", true); 
                        
                        if(u.getTipo() == 'C'){
                            System.out.println("é cliente");
                            rd = getServletContext().getRequestDispatcher("/ClienteServlet?action=list");   
                            rd.forward(request, response);  
                        }else if(u.getTipo() == 'F'){
                            System.out.println("é funcionário");
                            rd = getServletContext().getRequestDispatcher("/UsuarioServlet?action=list");   
                            rd.forward(request, response);
                        }                      
                        return;
                    }else{
                        rd = getServletContext().getRequestDispatcher("/index.jsp");        
                        String msg = "Usuário/senha incorretos.";
                        request.setAttribute("msg", msg);
                        rd.forward(request, response);
                        return;
                    }                    
                }
            }else if ( "list".equalsIgnoreCase(action) ){
                rd = getServletContext().getRequestDispatcher("/HotelServlet?action=pagination&recordsPerPage=5&currentPage=1");   
                rd.forward(request, response);
            }else{
                //System.out.println("Usuario já está logado!");  
                Usuario user = (Usuario)session.getAttribute("Usuario");
                 
                switch (user.getTipo()) {
                    case 'C':
                        //System.out.println("é cliente");
                        rd = getServletContext().getRequestDispatcher("/ClienteServlet?action=list");
                        rd.forward(request, response);
                        break;
                    case 'F':
                        //System.out.println("é funcionário");
                        rd = getServletContext().getRequestDispatcher("/UsuarioServlet?action=list");
                        rd.forward(request, response);
                        break;
                    default:
                        break;
                }
            }
        }catch(Exception e){
            rd = getServletContext().getRequestDispatcher("/erro.jsp");        
            String msg = "Usuário/senha incorretos.";
            request.setAttribute("msg", msg);
            rd.forward(request, response);
            return;
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
